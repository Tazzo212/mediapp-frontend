import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Signos } from 'src/app/_model/signos';
import { SignosService } from 'src/app/_service/signos.service';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-signos-vitales-edicion',
  templateUrl: './signos-vitales-edicion.component.html',
  styleUrls: ['./signos-vitales-edicion.component.css']
})
export class SignosVitalesEdicionComponent implements OnInit {

  form: FormGroup;
  id: number;
  edicion: boolean;
  pacientes$: Observable<Paciente[]>;
  constructor(
    private pacienteService : PacienteService,
    private route: ActivatedRoute,
    private router: Router,
    private signosService: SignosService
  ) { }

  ngOnInit(): void {
    this.pacientes$ = this.pacienteService.listar();
    this.form = new FormGroup({
      'idSignosVitales': new FormControl(0),
      'pulso': new FormControl(''),
      'ritmoRespiratorio': new FormControl(''),
      'temperatura': new FormControl(''),
      'idPaciente': new FormControl(''),
      'fecha': new FormControl('')
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });
  }

  private initForm() {
    if (this.edicion) {
      this.signosService.listarPorId(this.id).subscribe(data => {

        this.form = new FormGroup({
          'idSignosVitales': new FormControl(data.idSignosVitales),
          'pulso': new FormControl(data.pulso),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio),
          'temperatura': new FormControl(data.temperatura),
          'idPaciente': new FormControl(data.paciente.idPaciente),
          'fecha': new FormControl(data.fecha)
        });
      });
    }
  }

  get f() {
    return this.form.controls;
  }

  operar() {
    if (this.form.invalid) { return; }

    let signo = new Signos();
    signo.idSignosVitales = this.form.value['idSignosVitales'];
    signo.pulso = this.form.value['pulso'];
    signo.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];
    signo.temperatura = this.form.value['temperatura'];
    signo.idPaciente = this.form.value['idPaciente'];
    signo.fecha = moment(this.form.value['fecha']).format('YYYY-MM-DDTHH:mm:ss');

    if (this.edicion) {
      //MODIFICAR
      //PRACTICA COMUN
      /*this.signosService.modificar(signo).subscribe(() => {
        this.signosService.listar().subscribe(data => {
          this.signosService.setSignoCambio(data);
          this.signosService.setMensajeCambio('SE MODIFICO');
        });
      });*/
      //PRACTICA IDEAL
      this.signosService.modificar(signo).pipe(switchMap(() => {
        return this.signosService.listar();
      }))
      .subscribe(data => {
        this.signosService.setSignoCambio(data);
        this.signosService.setMensajeCambio('SE MODIFICO');
      });

    } else {
      //REGISTRAR
      this.signosService.registrar(signo).subscribe(() => {
        this.signosService.listar().subscribe(data => {
          this.signosService.setSignoCambio(data);
          this.signosService.setMensajeCambio('SE REGISTRO');
        });
      });
    }

    this.router.navigate(['signos-vitales']);

  }
}
