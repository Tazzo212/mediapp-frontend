import { environment } from './../../../environments/environment';
import { Component, Inject, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Perfil } from 'src/app/_model/perfil';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  perfil: Perfil;
  constructor(private dialogRef: MatDialogRef<PerfilComponent>,
    @Inject(MAT_DIALOG_DATA) private data: Perfil) { }

  ngOnInit(): void {
    this.perfil = new Perfil();
    this.perfil.usuario = this.data.usuario;
    this.perfil.rol = this.data.rol;

  }

  cerrar(){
    this.dialogRef.close();
  }

}
