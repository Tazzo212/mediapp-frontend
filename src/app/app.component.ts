import { Component, OnInit } from '@angular/core';
import { Menu } from './_model/menu';
import { LoginService } from './_service/login.service';
import { MenuService } from './_service/menu.service';
import { PerfilComponent } from 'src/app/pages/perfil/perfil.component';
import { Perfil } from 'src/app/_model/perfil';
import { MatDialog } from '@angular/material/dialog';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from './../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  menus: Menu[];
  perfil: Perfil;
  constructor(
    private menuService : MenuService,
    public loginService: LoginService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.menuService.getMenuCambio().subscribe(data => {
      this.menus = data;
    });
  }

  abrirDialogo() {
    const helper = new JwtHelperService();
    let token = sessionStorage.getItem(environment.TOKEN_NAME);
    const decodedToken = helper.decodeToken(token);

    this.perfil=new Perfil();
    this.perfil.usuario = decodedToken.user_name;
    this.perfil.rol = decodedToken.authorities;

    this.dialog.open(PerfilComponent, {
      data: this.perfil
    });
  }

}
