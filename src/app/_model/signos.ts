import { Paciente } from "./paciente";

export class Signos {
  idSignosVitales: number;
  idPaciente: number;
  fecha: string; //2020-11-07T11:30:05 ISODate || moment.js
  pulso: string;
  ritmoRespiratorio: string;
  temperatura: string;
  paciente: Paciente;
}
